import random

print("python guess-my-birthday.py")
name = input("Hi! What is your name?  ")
print(f"HI {name} ")

for guess in range(1, 6):
    number_month = random.randint(1, 12)
    number_year = random.randint(1924, 2004)
    answer=input(f"Guess {guess} : {name} were you born in {number_month} / {number_year}? You can answer Yes/No:  ").lower()
    if answer == "no":
        print("Drat! Lemme try again!")
    elif guess == 5:
        print("I have other thing to do. Good bye.")
        break
    else:
        print("I knew it!")
        break
